import sqlite3, sys, json
arguments = sys.argv
module = arguments[1]

print("Converting module \"{}\" to JSON".format(module))

con = sqlite3.connect(module)
cursor = con.execute("""
    SELECT * FROM info;
""")
results = cursor.fetchall()

# print(results[0]);

for i in results:
    jsonObject = json.dumps(i)
    print(jsonObject)